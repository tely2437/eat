import React, {Component, useEffect} from 'react'
import {AppLoading} from "expo";
import {useFonts} from 'expo-font';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from './src/Redux/store';
import Toast from 'react-native-toast-message';
import Navigation from './src/Navigation/AppNavigation'

import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';

import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const App = () => {

    const [loaded] = useFonts({
        SFUIDisplayBold: require('./src/Fonts/SFUIDisplay-Bold.ttf'),
        SFUIDisplayRegular: require('./src/Fonts/SFUIDisplay-Regular.ttf'),
        SFUIDisplaySemiBold: require('./src/Fonts/SFUIDisplay-Semibold.ttf'),
        SFUIDisplayMedium: require('./src/Fonts/SFUIDisplay-Medium.ttf'),
        'Roboto_medium': {
            uri: require('./src/Fonts/Roboto-Medium.ttf'),
        },
    });

    if (!loaded) {
        return null;
    }


    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <Navigation/>
                <Toast ref={(ref) => Toast.setRef(ref)} />
            </PersistGate>
        </Provider>
    );
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
});


export default App;