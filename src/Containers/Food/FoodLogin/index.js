import React, { Component } from "react";
import {
  Platform,
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  BackHandler,
  TextInput,
  I18nManager, Alert, AsyncStorage,
} from "react-native";
import { Content } from "native-base";
import { Metrics, Images } from "../../../Themes";
import styles from "./style";
import EvilIcons from "react-native-vector-icons/EvilIcons";

const food_login_bg =
  "http://antiquerubyreact.aliansoftware.net/all_live_images/ic_food_login_bg.png";

export default class FoodLogin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: "",
      password: "",
    };
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  backPressed = () => {
    this.props.navigation.navigate("FirstScreen");
    return true;
  };

  render() {
    StatusBar.setBarStyle("light-content", true);
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("#000000", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <View style={styles.mainView}>
        <ImageBackground source={{ uri: food_login_bg }} style={styles.mainImg}>
          <Image source={Images.food_logo} style={styles.logoImg} />

          <View style={styles.middleMainView}>
            <View>
              <TextInput
                ref="username"
                style={styles.infoText}
                maxLength={30}
                placeholder="Username"
                placeholderTextColor="#757575"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                keyboardType="default"
                textAlign={I18nManager.isRTL ? "right" : "left"}
                value={this.state.userName}
                onChangeText={(userName) =>
                  this.setState({ userName: userName })
                }
                onSubmitEditing={(event) => {
                  this.refs.password.focus();
                }}
                returnKeyType="next"
              />

              <View style={styles.firstDivider} />

              <TextInput
                secureTextEntry
                ref="password"
                style={styles.infoText}
                maxLength={16}
                placeholder="Password"
                placeholderTextColor="#757575"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                keyboardType="default"
                textAlign={I18nManager.isRTL ? "right" : "left"}
                value={this.state.password}
                onChangeText={(password) =>
                  this.setState({ password: password })
                }
                returnKeyType="done"
              />

              <View style={styles.secondDivider} />

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("TabView")}
                style={[
                  styles.btnBg,
                  {
                    backgroundColor: "#F05522",
                    marginTop: Metrics.HEIGHT * 0.1,
                  },
                ]}
              >
                <Text style={styles.btnText}>CONNEXION</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.bottomMainView}>
            <TouchableOpacity
              style={[styles.btnBg, { backgroundColor: "#2A64D1" }]}
            >
              <EvilIcons name="sc-facebook" color="#FFFFFF" size={30} />
              <Text style={styles.btnText}>CONNEXION AVEC FACEBOOK</Text>
            </TouchableOpacity>

            <Text style={styles.signUpText}>
              Vous n'avez de compte?{" "}
              <Text style={{ color: "#F05522" }}>Inscrivez vous</Text>
            </Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
