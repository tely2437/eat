import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  Platform,
  ImageBackground,
  ScrollView,
} from "react-native";

import styles from "./styles";
import { Metrics, Images } from "../../../Themes";
import AntDesign from "react-native-vector-icons/AntDesign";

import Entypo from "react-native-vector-icons/Entypo";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Fontisto from "react-native-vector-icons/Fontisto";

export default class Profiles extends Component {
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.navigate("FoodLogin");
    return true;
  };

  render() {
    StatusBar.setBarStyle("light-content", true);
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("#000000", true);
      StatusBar.setTranslucent(true);
    }
    return (
      <View style={styles.mainView}>
        <ImageBackground source={Images.ProfileBG} style={styles.ProfileBG}>
          <TouchableOpacity
            style={styles.Settingicon}
            underlayColor="transparent"
          >
            <Fontisto
              name="player-settings"
              size={25}
              color="#fff"
              style={{ alignSelf: "flex-end", top: 5, right: 10 }}
            />
          </TouchableOpacity>

          <View style={styles.MainProfileDetails}>
            <View style={{ flexDirection: "row" }}>
              <Image
                source={Images.ProfileUserImg}
                style={styles.ProfileUserImg}
              />
              <View style={{ alignSelf: "center" }}>
                <Text style={styles.ProfileUserName}>Tely Isma</Text>
                <Text style={styles.ProfileUserAdd}>
                  79 rue xyz Valenciennes
                </Text>
              </View>
            </View>
          </View>
        </ImageBackground>

        <View
          style={[
            styles.OrderHistroyMainBg,
            { marginTop: Metrics.HEIGHT * 0.02 },
          ]}
        >
          <View style={{ flexDirection: "row" }}>
            <View
              style={[
                styles.OrderHistoryCircle,
                { backgroundColor: "#4a90e2" },
              ]}
            >
              <Entypo
                name="text-document"
                size={20}
                color={"#fff"}
                style={{ alignSelf: "center" }}
              />
            </View>
            <Text style={styles.OrderHistoryText}>Historique de reservations</Text>
          </View>

          <View style={{ flexDirection: "row" }}>
            <AntDesign
              name="right"
              size={20}
              color="#c2c4ca"
              style={{ alignSelf: "center" }}
            />
          </View>
        </View>

        <View style={styles.MainProfileDetail}>
          <ScrollView>
            <View style={styles.OrderHistroyMainBg}>
              <TouchableOpacity
                style={{ flexDirection: "row" }}
                onPress={() => {}}
              >
                <View
                  style={[
                    styles.OrderHistoryCircle,
                    { backgroundColor: "#87bf4a" },
                  ]}
                >
                  <MaterialIcons
                    name="payment"
                    size={20}
                    color={"#fff"}
                    style={{ alignSelf: "center" }}
                  />
                </View>
                <Text style={styles.OrderHistoryText}>Methodes de Payment</Text>
              </TouchableOpacity>

              <View style={{ flexDirection: "row" }}>
                <AntDesign
                  name="right"
                  size={20}
                  color="#c2c4ca"
                  style={{ alignSelf: "center" }}
                />
              </View>
            </View>
            <View style={styles.BorderHorizontal} />

            <View style={styles.OrderHistroyMainBg}>
              <View style={{ flexDirection: "row" }}>
                <View
                  style={[
                    styles.OrderHistoryCircle,
                    { backgroundColor: "#f05522" },
                  ]}
                >
                  <FontAwesome
                    name="trophy"
                    size={20}
                    color={"#fff"}
                    style={{ alignSelf: "center" }}
                  />
                </View>
                <Text style={styles.OrderHistoryText}>Codes promo</Text>
              </View>

              <View style={{ flexDirection: "row" }}>
                <AntDesign
                  name="right"
                  size={20}
                  color="#c2c4ca"
                  style={{ alignSelf: "center" }}
                />
              </View>
            </View>
            <View style={styles.BorderHorizontal} />

            <View style={styles.OrderHistroyMainBg}>
              <View style={{ flexDirection: "row" }}>
                <View
                  style={[
                    styles.OrderHistoryCircle,
                    { backgroundColor: "#f5a623" },
                  ]}
                >
                  <Fontisto
                    name="player-settings"
                    size={20}
                    color="#fff"
                    style={{ alignSelf: "center" }}
                  />
                </View>
                <Text style={styles.OrderHistoryText}>Parametres</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <AntDesign
                  name="right"
                  size={20}
                  color="#c2c4ca"
                  style={{ alignSelf: "center" }}
                />
              </View>
            </View>
            <View style={styles.BorderHorizontal} />

            <View style={styles.OrderHistroyMainBg}>
              <View style={{ flexDirection: "row" }}>
                <View
                  style={[
                    styles.OrderHistoryCircle,
                    { backgroundColor: "#50e3c2" },
                  ]}
                >
                  <FontAwesome5
                    name="user-plus"
                    size={20}
                    color={"#fff"}
                    style={{ alignSelf: "center" }}
                  />
                </View>
                <Text style={styles.OrderHistoryText}>Inviter des Amis</Text>
              </View>

              <View style={{ flexDirection: "row" }}>
                <AntDesign
                  name="right"
                  size={20}
                  color="#c2c4ca"
                  style={{ alignSelf: "center" }}
                />
              </View>
            </View>
            <View style={styles.BorderHorizontal} />
          </ScrollView>
        </View>
      </View>
    );
  }
}
