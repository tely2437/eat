const images = {

  menu: require("../Images/menu.png"),
  food_login_bg: require("../Images/ic_food_login_bg.png"),
  food_logo: require("../Images/logo22.png"),
  seletedstar: require("../Images/seletedstar.png"),
  MapView: require("../Images/MapView.png"),
  Welcome_mainBg: require("../Images/Welcome_mainBg.png"),
  ProfileImg: require("../Images/profil.png"),
  ProfileUserImg: require("../Images/profil.png"),
  delivery_boy: require("../Images/ic_delivery_boy.png"),
  food_image: require("../Images/bg-homepage-egsalad.jpg"),

};

export default images;
