import { Dimensions } from "react-native";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import { createAppContainer } from "react-navigation";
const { width, height } = Dimensions.get("window");

import SearchResultOne from "../Containers/Food/SearchResultOne/index";
import FoodLogin from "../Containers/Food/FoodLogin/index";
import ProductDetails from "../Containers/Food/ProductDetails/index";
import TabView from "../Containers/Food/TabView/index";
import BookTable from "../Containers/Food/BookTable/index";
import AllCategories from "../Containers/Food/AllCategories/index";
import SearchScreen from "../Containers/Food/SearchScreen/index";

const MainStackFood = createStackNavigator(
  {
    FoodLogin: { screen: FoodLogin },
    SearchResultOne: { screen: SearchResultOne },
    ProductDetails: { screen: ProductDetails },
    TabView: { screen: TabView },
    BookTable: { screen: BookTable },
    AllCategories: { screen: AllCategories },
    SearchScreen: { screen: SearchScreen },
  },
  {
    headerMode: "none",
    navigationOptions: {
      gesturesEnabled: false,
    },
  }
);
/*Food Drawer END*/


const PrimaryNav = createStackNavigator(
  {
    MainStackFood: { screen: MainStackFood },
  },
  {
    headerMode: "none",
    initialRouteName: "MainStackFood",
    gesturesEnabled: false,
  }
);


export default createAppContainer(PrimaryNav);
