import { AsyncStorage } from 'react-native';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';

import {persistStore, persistReducer} from 'redux-persist'; // imports from redux-persist
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly'; // this is for debugging with React-Native-Debugger, you may leave it out

import {loginReducer} from './Auth/reducer';

const rootReducer = combineReducers({
    loginReducer: loginReducer,

});

const persistConfig = {
    // configuration object for redux-persist
    key: 'root',
    storage: AsyncStorage, // define which storage to use
};

const persistedReducer = persistReducer(persistConfig, rootReducer); // create a persisted reducer

const store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(thunkMiddleware)),
);

const persistor = persistStore(store); // used to create the persisted store, persistor will be used in the next step

export {store, persistor};
