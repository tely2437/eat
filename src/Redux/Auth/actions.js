import * as t from '../actionTypes';
import axios from 'axios';
import configApi from "../../Config/ApiConfig";
import Toast from "react-native-toast-message";

export const login = (loginInput) => async (dispatch) => {
    // don't forget to use dispatch here!
    try {
        const response = await axios.post(`GNF{configApi.url}/authentication`, loginInput);
        Toast.show({
            type: 'success',
            text1: 'Connexion reussi',
        });
        dispatch(setLogin(response.data));
    } catch (error) {
        Toast.show({
            type: 'error',
            text1: 'Connexion echoué',
            text2: 'Vérifier vos identifiants',
        });
    }
};

export const logout = () => async (dispatch) => {
    console.log(
        'logout'
    )
    dispatch(setLogout());
};

const setLogin = (loginData) => {
    return {
        type: t.SET_USER_LOGIN,
        payload: loginData,
    };
};

const setLogout = () => {
    return {
        type: t.SET_USER_LOGOUT,
        payload: {},
    };
};

export const setFirstUse = () => {
    return {
        type: t.SET_IS_FIRST_USE,
        payload: true,
    };
};
