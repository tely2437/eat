import {initialState} from './initialState';
import * as t from '../actionTypes';

export const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case t.SET_USER_LOGIN:
            return {
                ...state,
                ...action.payload, // this is what we expect to get back from API call and login page input
                isLoggedIn: true, // we set this as true on login
            };
        case t.SET_USER_LOGOUT:
            return {
                isLoggedIn: false, // we set this as true on login
                isFirstUse: state.isFirstUse
            };
        case t.SET_IS_FIRST_USE:
            return {
                ...state,
                ...action.payload, // this is what we expect to get back from API call and login page input
                isFirstUse: action.payload, // we set this as true on login
            };
        default:
            return state;
    }
};




